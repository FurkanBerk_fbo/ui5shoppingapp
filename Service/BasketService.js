var Database_Name = 'Shooping Application';
var Version = 1.0;
var Text_Description = ' Otomasyon Proje';
var Database_Size = 2 * 1024 * 1024;
var veriler = openDatabase(Database_Name, Version, Text_Description, Database_Size);

class BasketListService{

    creatBasketTable(){

        var basket=new Promise(function (resolve) {
            veriler.transaction(function(tx) {
                tx.executeSql('CREATE TABLE IF NOT EXISTS BasketTable(userLoginid,basketStock,prdctbasketid, FOREIGN KEY(prdctbasketid) REFERENCES ProductTable(productid))', [],
                    function(tx, result) {
                        resolve(true)
                        console.log("BasketTable TABLO OLUŞTURULDU")

                    },
                    function(tx, result) {
                        resolve(false)
                        console.log("BasketTable TABLO OLUŞTURULMADI")
                    })
            })
            
        })

        return basket
    }

    addBasketTable(loginuserinfo,stock,productid ) {

        var basket = new Promise(function(resolve) {

            veriler.transaction(function(tx) {
                tx.executeSql("INSERT INTO BasketTable(userLoginid,basketStock,prdctbasketid ) VALUES(?,?,?)", [loginuserinfo,stock,productid ],
                    function(tx, result) {
                        resolve(true)
                        console.log("ürün sepete eklendi")

                    },
                    function(tx, result) {
                        resolve(false)
                        console.log("ürün sepete başarısız ekleme")
                    })
            })
        });
        return basket;
    }

    deleteBasketTable(productid,loginuserid) {
        var basket = new Promise(function(resolve) {

            veriler.transaction(function(tx) {
                tx.executeSql('delete from BasketTable where prdctbasketid=? and userLoginid=?', [productid,loginuserid],
                    function(tx, result) {
                        resolve(true)
                        console.log("ÜRÜN SEPETTEN  SİLİNDİ SERVER")

                    },
                    function(tx, result) {
                        resolve(false)
                        console.log("ÜRÜN BAŞARISIZ SİLME SERVER")
                    })
            })
        });
        return basket;

    }
    deleteAllBasketTable(loginid) {
        var basket = new Promise(function(resolve) {

            veriler.transaction(function(tx) {
                tx.executeSql('delete from BasketTable where userLoginid=?', [loginid],
                    function(tx, result) {
                        resolve(true)
                        console.log("SEPET  SİLİNDİ SERVER")

                    },
                    function(tx, result) {
                        resolve(false)
                        console.log("SEPET SİLME HATALıs SERVER")
                    })
            })
        });
        return basket;

    }
    selectBasketTable(loginuserid) {
debugger
        var basket = new Promise(function(resolve) {

            veriler.transaction(function(tx) {
                tx.executeSql('SELECT *from  BasketTable,ProductTable,CategoryTable where BasketTable.prdctbasketid=Producttable.productid and Producttable.productCategory=CategoryTable.categoryid and userLoginid=? ', [loginuserid],
                    function(tx, result) {

                        resolve(result.rows)
                    },
                    function(tx, error) {
                        resolve(false)
                     
                    })
            })
        });
        return basket;



    }
    selectProductTable(productid) {

        var basket = new Promise(function(resolve) {

            veriler.transaction(function(tx) {
                tx.executeSql('SELECT *from  ProductTable where productid=? ', [productid],
                    function(tx, result) {

                        resolve(result.rows)
                    },
                    function(tx, error) {
                        resolve(false)
                     
                    })
            })
        });
        return basket;



    }
    
    updateProductTable( pstock,pid){

        
        var product=new Promise(function(resolve){

            veriler.transaction(function(tx){
                tx.executeSql("Update ProductTable set productStock=? where productid=?",[ pstock,pid],
                function (tx,result){
                    resolve(true)
                },
                function(tx,result){
                    resolve(false)
                })
            })
        })
        return product;
    }
    deleteBasketTable(productid,loginuserid){
       
        var basket=new Promise(function(resolve){
            veriler.transaction(function(tx){
            tx.executeSql("Delete from BasketTable where prdctbasketid=? and userLoginid=?",[productid,loginuserid],

            function(tx,result){
                          resolve(true)
            },
            function(tx,result){
            resolve(false)
            })
            })
        })
  return basket
        


    }
    updateBasketTable(productStock, productid,loginuserid) {
        var urun = new Promise(function(resolve) {

            veriler.transaction(function(tx) {
                tx.executeSql("UPDATE BasketTable  set basketStock=? where prdctbasketid=? and userLoginid=? ", [productStock, productid,loginuserid],
                    function(tx, result) {
                        resolve(true)
                        console.log("SEPET ADETİ GÜNCELLENDİ SERVER")

                    },
                    function(tx, result) {
                        resolve(false)
                        console.log("SEPET GÜNCELLEME HATASI SERVER")
                    })
            })
        });
        return urun;


    }

    selectBasketTableExist(loginuserid,productid){
        var basket = new Promise(function(resolve) {

            veriler.transaction(function(tx) {
                tx.executeSql('SELECT *from  BasketTable where userLoginid=? and prdctbasketid=?', [loginuserid,productid],
                    function(tx, result) {

                        resolve(result.rows)
                    },
                    function(tx, error) {
                        resolve(false)
                     
                    })
            })
        });
        return basket;
        
    }
}