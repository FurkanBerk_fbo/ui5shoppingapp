var Database_Name = 'Shooping Application';
var Version = 1.0;
var Text_Description = ' Otomasyon Proje';
var Database_Size = 2 * 1024 * 1024;
var veriler = openDatabase(Database_Name, Version, Text_Description, Database_Size);


    class ProductListService{
    creatProductTable() {
      

            var product = new Promise(function(resolve) {

                veriler.transaction(function(calis) {
                    calis.executeSql('CREATE TABLE IF NOT EXISTS ProductTable(productid INTEGER PRIMARY KEY,productName,productStock,productCategory,productPrice,productimage,FOREIGN KEY(productCategory) REFERENCES CategoryTable(categoryid))', [],
                        function(tx, result) {
                            resolve(true)
                            console.log(" ProductTable TABLO OLUŞTURULDU")

                        },
                        function(tx, result) {
                            resolve(false)
                            console.log("ProductTable TABLO OLUŞTURULMADI")
                        })
                })
            });
            return product;

        }
       

   
     addProduct(pname, pstock, pcategory,pprice,pimg) {

        var product = new Promise(function(resolve) {

            veriler.transaction(function(tx) {
                tx.executeSql("INSERT INTO ProductTable(productName,productStock,productCategory,productPrice,productimage) VALUES(?,?,?,?,?)", [pname, pstock, pcategory,pprice,pimg],
                    function(tx, result) {
                        resolve(true)
                      

                    },
                    function(tx, error) {
                        resolve(false)
                      
                    })
            })
        });
        return product;
    }

    selectProduct() {

        var product = new Promise(function(resolve) {

            veriler.transaction(function(tx) {
                tx.executeSql('SELECT *from  ProductTable,CategoryTable where ProductTable.productCategory=CategoryTable.categoryid ', [],
                    function(tx, result) {

                        resolve(result.rows)
                    },
                    function(tx, error) {
                        resolve(false)
                     
                    })
            })
        });
        return product;



    }
    selectProductCategory(selectcategory) {

        var product = new Promise(function(resolve) {

            veriler.transaction(function(tx) {
                tx.executeSql('SELECT* From  ProductTable,CategoryTable where ProductTable.productCategory=CategoryTable.categoryid and categoryName=?', [selectcategory],
                    function(tx, result) {

                        resolve(result.rows)
                    },
                    function(tx, error) {
                        resolve(false)
                     
                    })
            })
        });
        return product;



    }

     updateProduct(pname, pstock, pcategory,pprice,pimg,pid){

        var product=new Promise(function(resolve){

            veriler.transaction(function(tx){
                tx.executeSql("Update ProductTable set productName=?,productStock=?,productCategory=? ,productPrice=?,productimage=? where productid=?",[pname, pstock, pcategory,pprice,pimg,pid],
                function (tx,result){
                    resolve(true)
                },
                function(tx,result){
                    resolve(false)
                })
            })
        })
        return product;
    }
    
     deleteProduct(productid){
       
        var product=new Promise(function(resolve){
            veriler.transaction(function(tx){
            tx.executeSql("Delete from ProductTable where productid=?",[productid],

            function(tx,result){
                          resolve(true)
            },
            function(tx,result){
            resolve(false)
            })
            })
        })
  return product
        


    }

    selectProductCategoryCount() {

        var product = new Promise(function(resolve) {

            veriler.transaction(function(tx) {
                tx.executeSql('SELECT categoryName ,Count(*) AS [sayi] from  ProductTable,CategoryTable where ProductTable.productCategory=CategoryTable.categoryid  GROUP BY productCategory ', [],
                    function(tx, result) {

                        resolve(result.rows)
                    },
                    function(tx, error) {
                        resolve(false)
                     
                    })
            })
        });
        return product;



    }
  
}
        
    
   
