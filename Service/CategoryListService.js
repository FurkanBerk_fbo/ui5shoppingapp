var Database_Name = 'Shooping Application';
var Version = 1.0;
var Text_Description = ' Otomasyon Proje';
var Database_Size = 2 * 1024 * 1024;
var veriler = openDatabase(Database_Name, Version, Text_Description, Database_Size);

class CategoryListService {
    createCategoryTable() {

        var category = new Promise(function (resolve) {

            veriler.transaction(function (tx) {
                tx.executeSql('CREATE TABLE IF NOT EXISTS CategoryTable(categoryid INTEGER PRIMARY KEY,categoryName)', [],
                    function (tx, result) {
                        resolve(true)
                        console.log(" CategoryTable TABLO OLUŞTURULDU")

                    },
                    function (tx, result) {
                        resolve(false)
                        console.log("CategoryTable TABLO OLUŞTURULMADI")
                    })


            })

        })
        return category;

    }
    addCategoryTable(cName) {

        var category = new Promise(function (resolve) {

            veriler.transaction(function (tx) {
                tx.executeSql('INSERT INTO CategoryTable (categoryName) VALUES(?)', [cName],
                    function (tx, result) {
                        resolve(true)


                    },
                    function (tx, error) {
                        resolve(false)

                    })

            })

        })
        return category
    }
    selectCategory(){

        var category=new Promise(function (resolve) {

            veriler.transaction(function (tx) {
                tx.executeSql('Select*From CategoryTable',[],

                   function (tx, result) {
                        resolve(result.rows)


                    },
                    function (tx, error) {
                        resolve(false)

                    })
            })
            
        })
    return category
    }

    updateCategoryTable(cName,cid){

        var category=new Promise(function (resolve) {
            veriler.transaction(function (tx) {
                tx.executeSql('Update CategoryTable set categoryName=? where categoryid=? ',[cName,cid],

                function (tx,result){
                    resolve(true)
                },
                function(tx,result){
                    resolve(false)
                })
                
            })
            
        })
        return category
    }
    deleteCategoryTable(categoryid){
         
        var category=new Promise(function(resolve){
            veriler.transaction(function(tx){
            tx.executeSql("Delete from CategoryTable where categoryid=?",[categoryid],

            function(tx,result){
                          resolve(true)
            },
            function(tx,result){
            resolve(false)
            })
            })
        })
  return category
        


    }
}

