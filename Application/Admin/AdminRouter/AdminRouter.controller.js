sap.ui.define([
  "sap/ui/core/mvc/Controller",
], function (Controller) {
  "use strict";
  var base;
  return Controller.extend("com.UI5Starter.Application.Admin.AdminRouter.AdminRouter", {
    onInit: function () {
      base = this;
      base.getView().setModel(oModel);
    },
    onSelectTab: function(oevent){

      var oTab = oevent.getParameter('item');
      var oRouter = sap.ui.core.UIComponent.getRouterFor(this); //  sayfa geçişleri için 
		
      if(oTab.mProperties.text=="ÜRÜN LİSTESİ"){
        oRouter.navTo("Admin/ProductList");
      }
      if(oTab.mProperties.text=="KATEGORİ LİSTESİ"){
        oRouter.navTo("Admin/CategoryList");
      }
      if(oTab.mProperties.text=="KULLANICI LİSTESİ"){
        oRouter.navTo("Admin/UserList");
      }

       

    },ongoLogin:function () {
      location.href="#/Admin/Login"
    }

  });
});
