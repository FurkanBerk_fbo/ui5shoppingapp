sap.ui.define([
  'com/UI5Starter/Application/Base/BaseController',
  "sap/ui/core/Core",
  "sap/ui/layout/HorizontalLayout",
  "sap/ui/layout/VerticalLayout",
  "sap/m/Dialog",
  "sap/m/DialogType",
  "sap/m/Button",
  "sap/m/ButtonType",
  "sap/m/Label",
  "sap/m/MessageToast",
  "sap/m/Text",
  "sap/m/TextArea"
], function (BaseController, Core, HorizontalLayout, VerticalLayout, Dialog, DialogType, Button, ButtonType, Label, MessageToast, Text, TextArea) {
  'use strict';
  var base;
  var userService = "", userdialogService = "", selectrow, apressDialog =""

  return BaseController.extend("com.UI5Starter.Application.Admin.UserList.controller.UserList", {
    onInit: function () {
      base = this;
      var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
      oRouter.getRoute("Admin/UserList").attachPatternMatched(this.onURLControlUserList, this);

      userdialogService = new UserListDialogService()
      userService = new UserListService()
      userService.creatUsersTable()
      this.GetAllDataUsers()
      this.onSetDialog()

    }, onURLControlUserList: function () {
      if (JSON.parse(localStorage.getItem("userlogin")).userdescrip == 0) {
        location.href = "#/UserShopping/UserProducts"
      }

    },

    onSetDialog: function () {

      if (!apressDialog) {

        var controllerdialog = sap.ui.controller("com.UI5Starter.Application.Admin.UserListDialog.controller.UserListDialog");
        apressDialog = sap.ui.xmlfragment("com.UI5Starter.Application.Admin.UserListDialog.fragment.UserListDialog", controllerdialog);

      }
    },
    UserDialogScreen: function () {



      apressDialog.open()
    }, UserEditDialogScreen: function (oEvent) {


      oModel.oData.userinputs.id = oEvent.getSource().getBindingContext().getProperty().uid


      oModel.oData.userinputs.name = oEvent.getSource().getBindingContext().getProperty().uname
      oModel.oData.userinputs.age = oEvent.getSource().getBindingContext().getProperty().uage
      oModel.oData.userinputs.eposta = oEvent.getSource().getBindingContext().getProperty().ueposta
      oModel.oData.userinputs.city = oEvent.getSource().getBindingContext().getProperty().ucity
      oModel.oData.userinputs.password = oEvent.getSource().getBindingContext().getProperty().upassword


      sap.ui.getCore().byId("btn1").setText("DÜZENLE");

      apressDialog.open()

    },
    GetAllDataUsers: function () {
      oModel.setProperty("/userinputs", {})

      userService.selectUserTable().then(function (res) {

        var userarr = []

        for (var i = 0; i < res.length; i++) {

          userarr.push({
            "uid": res[i].userid,
            "uname": res[i].userName,
            "uage": res[i].userAge,
            "ueposta": res[i].userEposta,
            "ucity": res[i].userCity,
            "upassword": res[i].userPassword
          })

        }
        oModel.setProperty("/usersvalue", userarr)

      })


    },

    onDelete: function (oEvent) {
     
      selectrow = oEvent.getSource().getBindingContext().getProperty().uid
      userService.deleteUserTable(selectrow).then(function (res) {

        if (res) {
          alert("KİŞİ SİLME BAŞARILI")
        } else {
          alert("KİŞİ HATALI SİLME")
        }

      })
      this.GetAllDataUsers()

    },



  })
});




