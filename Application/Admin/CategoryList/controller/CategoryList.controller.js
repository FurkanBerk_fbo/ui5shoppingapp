sap.ui.define([
  'com/UI5Starter/Application/Base/BaseController',
  "sap/m/MessageBox",
  "sap/ui/model/Filter",
  "sap/ui/model/FilterOperator",
  "sap/ui/model/FilterType",
], function (BaseController, MessageBox, Filter, FilterOperator) {
  'use strict';
  var base, categoryService = "", selectrow;
  return BaseController.extend("com.UI5Starter.Application.Admin.CategoryList.controller.CategoryList", {
    onInit: function () {
      var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
      oRouter.getRoute("Admin/CategoryList").attachPatternMatched(this.onURLControlCategoryList, this);
      categoryService = new CategoryListService()
      categoryService.createCategoryTable()
      this.selectCategoryList()
      this.onSetCategoryDialog()
      base = this;
    },
    onSelectTab: function (event) {

      ;
      alert("categgory press")



    }, onURLControlCategoryList: function () {
      if (JSON.parse(localStorage.getItem("userlogin")).userdescrip == 0) {
        location.href = "#/UserShopping/UserProducts"
      }

    },
    onSetCategoryDialog: function () {
      if (!this.cpressDialog) {

        var categorycontroller = sap.ui.controller("com.UI5Starter.Application.Admin.CategoryListDialog.controller.CategoryListDialog");
        this.cpressDialog = sap.ui.xmlfragment("com.UI5Starter.Application.Admin.CategoryListDialog.fragment.CategoryListDialog", categorycontroller);

      }
    },
    CategoryDialogScreenOpen: function () {




      this.cpressDialog.open()
    },


    CategoryEditDialogScreen: function (oEvent) {


      // selectrow = oEvent.getSource().getBindingContext().getProperty().cid

      oModel.oData.categorys.id = oEvent.getSource().getBindingContext().getProperty().cid

      oModel.oData.categorys.name = oEvent.getSource().getBindingContext().getProperty().cName


      sap.ui.getCore().byId("cbtn").setText("DÜZENLE");

      this.cpressDialog.open()

    },
    // addCategoryName: function () {
    //   
    //   var model = oModel.oData.categorys
    //   categoryService.addCategoryTable(model.name).then(function (res) {

    //     if (res) {
    //       alert("KATEGORİ EKLEME BAŞARILI")
    //       oModel.setProperty("/categorys", {})

    //     } else {
    //       alert("HATALI KATEGORİ EKLEME")
    //     }

    //   })

    //   this.selectCategoryList()

    // },
    selectCategoryList: function () {
      oModel.setProperty("/categorys", {})
      categoryService.selectCategory().then(function (res) {
        var categoryarr = []
        
        for (var i = 0; i < res.length; i++) {

          categoryarr.push({
            "cid": res[i].categoryid,
            "cName": res[i].categoryName
          })

        }
        oModel.setProperty("/categoryvalues", categoryarr)


      })

    },
    // updateCategoryList: function () {
    //   var model = oModel.oData.categorys
    //   categoryService.updateCategoryTable(model.name, selectrow).then(function (res) {

    //     if (res) {
    //       alert("KATEGORİ GÜNCELLEMESİ BAŞARILI")
    //     } else {

    //       alert("HATALI KATEGORİ GÜNCELLEME")
    //     }

    //   })
    //   this.selectCategoryList()

    // },
    deleteCategoryList: function (oEvent) {
     
      selectrow = oEvent.getSource().getBindingContext().getProperty().cid
      MessageBox.warning("SİLMEK İSTİYOR MUSUNUZ?..", {
        actions: [MessageBox.Action.OK, MessageBox.Action.CANCEL],
        emphasizedAction: MessageBox.Action.OK,
        onClose: function (sAction) {
          if (sAction == "OK") {
            categoryService.deleteCategoryTable(selectrow).then(function (res) {
              
              if (res) {

                alert("KATEGORİ SİLME BAŞARILI")
                base.selectCategoryList()



              } else {
                alert("KATEGORİ HATALI SİLME")
              }

            })


          }


        }
      });


    },
    // onDialogButtonPress: function (oEvent) {
    //  
    //   if (oEvent.oSource.mProperties.text == "KATEGORİ EKLE") {
    //     this.addCategoryName()
    //     this.CategoryDialogScreenCancel()
    //   } else if (oEvent.oSource.mProperties.text == "DÜZENLE") {
    //     this.updateCategoryList()
    //     this.CategoryDialogScreenCancel()

    //   }
    //   else {
    //     alert("HATALI İŞLEM")

    //   }

    // },
    filterGlobally: function (oEvent) {
      var aFilter = [];
      var sQuery = oEvent.getParameter("query");
      var oList = this.getView().byId("table2")
      var oBinding = oList.getBinding("rows")

      if (sQuery) {
        aFilter.push(new Filter("cName", FilterOperator.Contains, sQuery))
      }
      oBinding.filter(aFilter)
    },



  })
});

//     addCategoryName:function() {
// 
//  var model=oModel.oData.categorys
//  var kontrol=0
//  categoryService.selectCategory().then(function (res) {


//    for(var i=0;i<res.length;i++){
//      
//       if(model.name==res[i].categoryName){
//         kontrol=1;
//         alert("BU KATEGORİ DAHA ÖNCEDEN MEVCUT -"+kontrol)
//       }
//    }

//    if(kontrol==0){

//    categoryService.addCategoryTable(model.name).then(function (res) {

//      if (res) {
//        alert("KATEGORİ EKLEME BAŞARILI")
//        oModel.setProperty("/categorys", {})

//      } else {
//        alert("HATALI KATEGORİ EKLEME")
//      }

//    })



//  }


//  })
//  this.selectCategoryList()

// }