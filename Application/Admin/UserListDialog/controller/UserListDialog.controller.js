sap.ui.define([
    'com/UI5Starter/Application/Base/BaseController',
	'sap/ui/core/Fragment',
	'sap/m/MessageToast'
], function(Controller) {
	"use strict";
   
	var userService="",userdialogService="",apressDialog=""
	return Controller.extend("com.UI5Starter.Application.Admin.UserListDialog.controller.UserListDialog", {
        onInit: function () {
			
          
		
		   userService.creatUsersTable()
          },
		  onAddnewUserDialog: function () {
			userdialogService = new UserListDialogService()
			var descripid=0
			var model = oModel.oData.userinputs
			userdialogService.addUsersTable(descripid,model.name, model.age, model.eposta, model.city, model.password).then(function (res) {
	  
			  if (res) {
				alert("KİŞİ EKLEME BAŞARILI")
				oModel.setProperty("/userinputs", {})
	  
			  } else {
				alert("HATALI KİŞİ EKLEME")
			  }
			})
	  
			this.GetAllDataUsers()
	  
	  
		  },
		  UserDialogScreenCancel: function () {
			  
			oModel.oData.userinputs.name=""
			oModel.oData.userinputs.age=""
			oModel.oData.userinputs.eposta=""
			oModel.oData.userinputs.city=""
			oModel.oData.userinputs.password=""
		 
			sap.ui.getCore().byId("btn1").setText("KİŞİLERİ KAYDET"); 
			sap.ui.getCore().byId("dialog").close()
			
		  },
		  onDialogButtonPress:function (oEvent) {
			
			if(oEvent.oSource.mProperties.text=="KİŞİLERİ KAYDET"){
			  this.onAddnewUserDialog()
			  this.UserDialogScreenCancel()
			}else if(oEvent.oSource.mProperties.text=="DÜZENLE"){
			  this.onUserUpdate()
			  this.UserDialogScreenCancel()
	  
			}
			else{
			  alert("HATALI İŞLEM")
	  
			}
			
		  },GetAllDataUsers: function () {
			userService = new UserListService()
			oModel.setProperty("/userinputs", {})
	  
			userService.selectUserTable().then(function (res) {
	  
			  var userarr = []
	  
			  for (var i = 0; i < res.length; i++) {
	  
				userarr.push({
				  "uid": res[i].userid,
				  "uname": res[i].userName,
				  "uage": res[i].userAge,
				  "ueposta": res[i].userEposta,
				  "ucity": res[i].userCity,
				  "upassword": res[i].userPassword
				})
	  
			  }
			  oModel.setProperty("/usersvalue", userarr)
	  
			})
	  
		},
		onUserUpdate: function () {
			
			userService = new UserListService()
				  var model = oModel.oData.userinputs
				  userService.updateUserTable(model.name, model.age, model.eposta, model.city, model.password,model.id).then(function (res) {
			
					if (res) {
					  alert("KİŞİ GÜNCELLEMESİ BAŞARILI")
					} else {
			
					  alert("HATALI KİSİ GÜNCELLEME")
					}
			
			
				  })
				  this.GetAllDataUsers();
				},
	

    });
});
