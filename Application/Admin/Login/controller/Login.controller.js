sap.ui.define([
    'com/UI5Starter/Application/Base/BaseController',
  ], function (BaseController) {
    'use strict';
    var base,loginControl="";
    return BaseController.extend("com.UI5Starter.Application.Admin.Login.controller.Login", {
      onInit: function () {
        base = this;
        loginControl=new UserListService()
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter.getRoute("Admin/Login").attachPatternMatched(this.onLoginLocalClear,this);
      
        
      },
       onLoginLocalClear:function () {
        localStorage.clear()
       },
      onUserLoginControl:function () {
        var kontrol=0;
        
        var username=this.getView().byId("inputusername").getValue();
        var sifre=this.getView().byId("inputsifre").getValue();
        loginControl.selectUserTable().then(function (res) {

          for(var i=0;i<res.length;i++){
              
            if(res[i].userName==username && res[i].userPassword==sifre){
              var obj=Object.assign({},res[i])
              var jsonvalue=JSON.stringify(obj)

              kontrol++
              if(username=="ADMİN" && sifre=="1"){
                localStorage.setItem("userlogin",jsonvalue)
                base.onEditInput()
                location.href="#/Admin/UserList"
              }
              else{
                localStorage.setItem("userlogin",jsonvalue)
                base.onEditInput()
              location.href="#/UserShopping/UserProducts"
              }
              }
           
          }
          if(kontrol==0){
            base.onEditInput()
            alert("GİRİLEN KULLANICI ADI VE ŞİFREDE KAYITLI KULLANICI YOKTUR LÜTFEN TEKRAR DENEYİNİZ")
          }
          
        })
      },onEditInput:function () {
        base.byId("inputusername").setValue("")
        base.byId("inputsifre").setValue("")
        
      }
      // onlogin:function () {
      //   location.href="#/Admin/UserList"
        
      // },
      // onloginuser:function () {
      //   location.href="#/UserShopping/UserProducts"
        
      // }
    })
  });