sap.ui.define([
    'com/UI5Starter/Application/Base/BaseController',
	'sap/ui/core/Fragment',
	'sap/m/MessageToast'
], function(Controller, Fragment, MessageToast) {
	"use strict";
  var categoryService=""

	return Controller.extend("com.UI5Starter.Application.Admin.CategoryListDialog.controller.CategoryListDialog", {
        onInit: function () {
            // base = this;
          },
          CategoryDialogScreenCancel: function () {
            oModel.oData.categorys.name = ""
      
            sap.ui.getCore().byId("cbtn").setText("KATEGORİ EKLE");
            sap.ui.getCore().byId("dialogcategory").close()
          },
          addCategoryName: function () {
            categoryService = new CategoryListService()
            
            var model = oModel.oData.categorys
            categoryService.addCategoryTable(model.name).then(function (res) {
      
              if (res) {
                alert("KATEGORİ EKLEME BAŞARILI")
                oModel.setProperty("/categorys", {})
      
              } else {
                alert("HATALI KATEGORİ EKLEME")
              }
      
            })
      
            this.selectCategoryList()
      
          },
          selectCategoryList: function () {
            categoryService = new CategoryListService()
            oModel.setProperty("/categorys", {})
            categoryService.selectCategory().then(function (res) {
              var categoryarr = []
              
              for (var i = 0; i < res.length; i++) {
      
                categoryarr.push({
                  "cid": res[i].categoryid,
                  "cName": res[i].categoryName
                })
      
              }
              oModel.setProperty("/categoryvalues", categoryarr)
      
      
            })
      
          },
          updateCategoryList: function () {
            categoryService = new CategoryListService()
            var model = oModel.oData.categorys
            categoryService.updateCategoryTable(model.name, model.id).then(function (res) {
      
              if (res) {
                alert("KATEGORİ GÜNCELLEMESİ BAŞARILI")
              } else {
      
                alert("HATALI KATEGORİ GÜNCELLEME")
              }
      
            })
            this.selectCategoryList()
      
          },
          onDialogButtonPress: function (oEvent) {
           
            if (oEvent.oSource.mProperties.text == "KATEGORİ EKLE") {
              this.addCategoryName()
              this.CategoryDialogScreenCancel()
            } else if (oEvent.oSource.mProperties.text == "DÜZENLE") {
              this.updateCategoryList()
              this.CategoryDialogScreenCancel()
      
            }
            else {
              alert("HATALI İŞLEM")
      
            }
      
          },
        
        });
    });
    