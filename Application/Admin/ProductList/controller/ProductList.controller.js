sap.ui.define([
  'com/UI5Starter/Application/Base/BaseController',
], function (BaseController) {
  'use strict';
  var base, productService = "", selectitem = "", image, categoryService = "";
  const tobase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
  return BaseController.extend("com.UI5Starter.Application.Admin.ProductList.controller.ProductList", {
    onInit: function () {
      var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
      oRouter.getRoute("Admin/ProductList").attachPatternMatched(this.onURLControlProductList, this);
      productService = new ProductListService()
      categoryService = new CategoryListService()
      productService.creatProductTable()
      this.onSelectList()
      this.selectCategoryListName()
      base = this;
    }, onURLControlProductList: function () {
      if (JSON.parse(localStorage.getItem("userlogin")).userdescrip == 0) {
        location.href = "#/UserShopping/UserProducts"
      }
      this.selectCategoryListName()
    },

    onAdd: function () {

      var model = oModel.oData.inputs
      var img = image || "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_640.png"

      productService.addProduct(model.name, model.stock, model.category, model.price, img).then(function (res) {
        if (res) {
          alert("ÜRÜN EKLEME BAŞARILI")
          oModel.setProperty("/inputs", {})

        } else {
          alert("HATALI ÜRÜN EKLEME")
        }
      })
      this.onSelectList()

    },
    onUpdate: function () {

      var model = oModel.oData.inputs
      var img = image || "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_640.png"

      productService.updateProduct(model.name, model.stock, model.category, model.price, img, selectitem).then(function (res) {
        if (res) {
          alert("KİŞİ GÜNCELLEMESİ BAŞARILI")
        } else {

          alert("HATALI KİSİ GÜNCELLEME")
        }
      })
      this.onSelectList()

    },
    onSelectList: function () {

      oModel.setProperty("/inputs", {})

      productService.selectProduct().then(function (res) {

        var productsarray = []
        for (var i = 0; i < res.length; i++) {
          productsarray.push({
            "pid": res[i].productid,
            "pname": res[i].productName,
            "pstock": res[i].productStock,
            "pcategory": res[i].categoryName,
            "pprice": res[i].productPrice,
            "image": res[i].productimage
          })
        }

        oModel.setProperty("/products", productsarray)

      })
    },
    selectCategoryListName: function () {
      oModel.setProperty("/selectcName", {})
      categoryService.selectCategory().then(function (res) {
        var categorynamearr = []

        for (var i = 0; i < res.length; i++) {

          categorynamearr.push({

            "cName": res[i].categoryName,
            "cid": res[i].categoryid
          })

        }
        oModel.setProperty("/selectcName", categorynamearr)


      })

    },
    onListClick: function (oEvent) {

      selectitem = oEvent.getSource().getBindingContext().getObject().pid

      var productName = oEvent.getSource().getBindingContext().getObject().pname
      var productStock = oEvent.getSource().getBindingContext().getObject().pstock
      var productCategory = oEvent.getSource().getBindingContext().getObject().pcategory
      var productPrice = oEvent.getSource().getBindingContext().getObject().pprice

      oModel.oData.inputs.name = productName;
      oModel.oData.inputs.stock = productStock;
      oModel.oData.inputs.category = productCategory;
      this.getView().byId("inputprice").setValue(productPrice)
      //  oModel.oData.inputs.price=productPrice;
    },
    onDelete: function () {


      productService.deleteProduct(selectitem).then(function (res) {
        if (res) {
          alert("ÜRÜN SİLME BAŞARILI")
        } else {
          alert("HATALI SİLME")
        }


      })
      this.onSelectList()
    }, onBaseConvert: function (oEvent) {




      const uploadedfile = oEvent.mParameters.files[0]

      tobase64(uploadedfile)
        .then(res => {

          image = res
        })
        .catch(err => {
          console.log(err)
        })




    }


  })
});