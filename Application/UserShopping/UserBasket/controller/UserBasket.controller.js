sap.ui.define([
    'com/UI5Starter/Application/Base/BaseController',
  ], function (BaseController) {
    'use strict';
    var base,basketService="",basketres,loginuserid="";
    return BaseController.extend("com.UI5Starter.Application.UserShopping.UserBasket.controller.UserBasket", {
      onInit: function () {
        
        base = this;
         var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
         oRouter.getRoute("UserShopping/UserBasket").attachPatternMatched(this.selectBasketGridList,this);
      
        basketService=new BasketListService()
        basketService.creatBasketTable()
         this.selectBasketGridList()
        

      },
      selectBasketGridList(){
       
        var  toplam=0
       
        oModel.setProperty("/basketitems",{})
         loginuserid=JSON.parse(localStorage.getItem("userlogin")).userid
        basketService.selectBasketTable(loginuserid).then(function (res) {

           basketres=res
          var basketlist=[]
         
          for(var i=0;i<res.length;i++){
           
            basketlist.push({
              "prdctid": res[i].productid,
              "prdctName": res[i].productName,
              "prdctStock": res[i].basketStock,
              "prdctCategory": res[i].categoryName,
              "prdctPrice": res[i].productPrice,
              "prdctimage": res[i].productimage
            })
          //  toplam odenecek tutar yazdırma
             toplam += ( parseInt(res[i].productPrice)* parseInt(res[i].basketStock))
           
          }
          oModel.setProperty("/basketitems",basketlist)
          
          
          base.byId("total").setText(toplam)
        })
          
         
         
         
       },
       deleteBasketGridList:function (oEvent) {
         
        var selectproduct= oEvent.getSource().getBindingContext().getProperty().prdctid

        basketService.deleteBasketTable(selectproduct,loginuserid).then(function (res) {
        
          if (res) {
            alert("ÜRÜN SEPETTEN SİLİNMİŞTİR")
        } else {
            alert("HATALI SİLME İŞLEMİ")
        }
          
        })

        this.selectBasketGridList()

         
       },
       plusBasketStock:function (oEvent) {
        var selectproductid= oEvent.getSource().getBindingContext().getProperty().prdctid
        var selectproductstock=oEvent.getSource().getBindingContext().getProperty().prdctStock
         
        selectproductstock+=1
        basketService.updateBasketTable(selectproductstock,selectproductid,loginuserid).then(function (res) {

          if (res) {
            console.log("STOK GÜNCELLEMESİ BAŞARILI")
          } else {
  
            console.log("STOK KİSİ GÜNCELLEME")
          }
  
          
        })
        this.selectBasketGridList()

       },
       minusBasketStock:function (oEvent) {
        var selectproductid= oEvent.getSource().getBindingContext().getProperty().prdctid
        var selectproductstock=oEvent.getSource().getBindingContext().getProperty().prdctStock
         
        if(selectproductstock>1){
        selectproductstock-=1
        }
        basketService.updateBasketTable(selectproductstock,selectproductid,loginuserid).then(function (res) {

          if (res) {
            console.log("STOK GÜNCELLEMESİ BAŞARILI")
          } else {
  
            console.log("STOK KİSİ GÜNCELLEME")
          }
  
          
        })
        this.selectBasketGridList()

       },
       
      onChange:function () {
       location.href="#/UserShopping/UserProducts"
      },
      payBasketProducts:function () {
        

    var selectedProducts = []
    for (var i = 0; i < basketres.length; i++) {

        selectedProducts.push(basketres.item(i).prdctbasketid)


    }
    var getProducts = selectedProducts.map(p => basketService.selectProductTable(p));
    var sepet = Object.assign([], basketres)
    var products = []
    Promise.all(getProducts).then(response => {
        console.log(response);
        products = response.map(r => Object.assign([], r)[0])

        products.forEach(p => {
            var cartProduct = sepet.find(s => s.prdctbasketid == p.productid)
            var newStock = p.productStock - cartProduct.basketStock
            console.log(p, newStock);
            basketService.updateProductTable(newStock, p.productid).then(function(res) {
                if (res) {
                    console.log("DOĞRU STOK GÜNCELLEME")
                } else {
                    alert("HATALI STOK GÜNCELLEME")
                }
            })

        })
        base.deleteuserBasket()
       
    })

        //   var urundeger=[]
        // for(var i=0;i<basketres.length;i++){
        //   // var selectproduct=basketres[i].productid
        //   // var basketstock=basketres[i].productStock
           
        //   urundeger.push({
        //     "selectprdct":basketres[i].productid,
        //     "basketstock": basketres[i].productStock
        //   })
        // }
        // for(var i=0;i<basketres.length;i++){
        //   basketService.selectProductTable(urundeger[i].selectprdct).then(function (val) {
        //    
            
        //     var newStock= parseInt(val[0].productStock) - parseInt(urundeger[i].basketstock)
        //   // var newStock= 100-basketstock

        //     basketService.updateProductTable(newStock, urundeger[i].selectprdct).then(function(aa) {
        //       if (aa) {
        //           alert("DOĞRU STOK GÜNCELLEME  "+newStock)
        //       } else {
        //          alert("HATALI STOK GÜNCELLEME")
        //       }
        //   })
            
        //   })
  
        // }
      
        

      },
      deleteuserBasket:function(){
         
        var loginuserid=JSON.parse(localStorage.getItem("userlogin")).userid
        basketService.deleteAllBasketTable(loginuserid).then(function(res){
          if (res) {
            alert("ÖDEME YAPILMIŞTIR")
          } else {
            alert("HATALI ÖDEME SEPET SİLME")
          }
        })
          
base.selectBasketGridList()
        
      },
      deleteBasketProduct(index) {
          // var sptrefdelete = new sptDeleteBasketProduct()
          secilenproductid = len.item(index).productid
          sepetservice.deletesepetUrun(secilenproductid).then(function(res) {
      
              if (res) {
                  alert("ÜRÜN SEPETTEN SİLİNMİŞTİR")
              } else {
                  alert("HATALI SİLME İŞLEMİ")
              }
      
          })
          sepeturunsirala()
      }
      
    })
  });