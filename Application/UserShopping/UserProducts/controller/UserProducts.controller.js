sap.ui.define([
    'com/UI5Starter/Application/Base/BaseController',
    "sap/f/Card",
    "sap/m/Label",
    "sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
  ], function (BaseController,Card,Label,Filter,FilterOperator) {
    'use strict';
    var base,categoryService="",productService="",basketService="",loginuserinfo="";
    return BaseController.extend("com.UI5Starter.Application.UserShopping.UserProducts.controller.UserProducts", {
      onInit: function () {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter.getRoute("UserShopping/UserProducts").attachPatternMatched(this.onURLControlUserProduct,this);
         categoryService=new CategoryListService()
        basketService=new BasketListService()
        productService=new ProductListService()
        basketService.creatBasketTable()
        this.selectCategoryListName()
        this.selectProductGridList()
        
     
       
       
        base = this;
      },
      onURLControlUserProduct:function(){
        this.selectProductGridList()
        if (localStorage.getItem("userlogin") == null) {
          location.href="#/Admin/Login"
      }
        
      }
     ,
      selectCategoryListName: function () {

        oModel.setProperty("/categorylist",{})
        productService.selectProductCategoryCount().then(function (res) {
          var categorynamearr=[]
          for(var i=0;i<res.length;i++){
            categorynamearr.push({
              "cName": res[i].categoryName,
              "cSayi": res[i].sayi
            })
          }
          oModel.setProperty("/categorylist", categorynamearr)
          
        })
      
  
      },
       selectProductGridList(){
        oModel.setProperty("/productitems",{})
        productService.selectProduct().then(function (res) {
          var productlist=[]

          for(var i=0;i<res.length;i++){
            
            productlist.push({
              "prdctid": res[i].productid,
              "prdctName": res[i].productName,
              "prdctStock": res[i].productStock,
              "prdctCategory": res[i].categoryName,
              "prdctPrice": res[i].productPrice,
              "prdctimage": res[i].productimage
            })
            
          }
          oModel.setProperty("/productitems",productlist)
          
        })
         
       },
      gologin:function () {
        localStorage.clear()
        oModel.setProperty("/basketitems",[])
        location.href="#/Admin/Login"
      },
      SearchCategory : function(oEvent) {
       
        var aFilter=[];
        var sQuery = oEvent.getParameter("query");
       var oList=this.getView().byId("categorylist")
     var   oBinding=oList.getBinding("items")
  
        if (sQuery) {
        aFilter.push(new Filter("cName",FilterOperator.Contains,sQuery))
        }
        oBinding.filter(aFilter)
  
        // this._filter();
      },
      onListClickCategory: function (oEvent) {
        
        oModel.setProperty("/productitems",{})
        var productCategory = oEvent.oSource.mProperties.title

        productService.selectProductCategory(productCategory).then(function (res) {

          var productlist=[]

          for(var i=0;i<res.length;i++){
            
            productlist.push({
              "prdctid": res[i].productid,
              "prdctName": res[i].productName,
              "prdctStock": res[i].productStock,
              "prdctCategory": res[i].categoryName,
              "prdctPrice": res[i].productPrice,
              "prdctimage": res[i].productimage
            })
            
          }
          oModel.setProperty("/productitems",productlist)
          
        })
        
      },
       onAddProductBasket:function (oEvent) {
         
       var prdctval= oEvent.getSource().getBindingContext().getProperty()
       loginuserinfo=JSON.parse(localStorage.getItem("userlogin")).userid
       var basketStock=1

         basketService.selectBasketTableExist(loginuserinfo, prdctval.prdctid).then(function (res) {

          if(res.length>0){

            var stockval=res[0].basketStock
            stockval+=1
            basketService.updateBasketTable(stockval,prdctval.prdctid,loginuserinfo).then(function (res) {
              if (res) {
                alert("BAŞARILI ADET GÜNCELLEME")
            } else {
                alert("HATALI ADET GÜNCELLEME")
            }
              
            })
          }else{
            basketService.addBasketTable(loginuserinfo,basketStock,prdctval.prdctid).then(function (res) {
              if (res) {
                alert("ÜRÜN EKLEME BAŞARILI")
                oModel.setProperty("/basketitems", {})
      
              } else {
                alert("HATALI ÜRÜN EKLEME")
              }
               
             })
          }
           
         })
     
      

       },
       ongoBasket:function () {
        location.href="#/UserShopping/UserBasket"
       }
         
      
    })
  });

  // oEvent.getSource().getBindingContext().getProperty()